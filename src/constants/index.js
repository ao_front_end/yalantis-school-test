const API_URL = 'https://yalantis-react-school.herokuapp.com/api/task0'
const COLOR = {
  GRAY: 'gray',
  BLUE: 'blue',
  GREEN: 'green',
  RED: 'red'
}

export {
  API_URL,
  COLOR
}