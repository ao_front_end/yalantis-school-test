import axios from 'axios'
import { API_URL } from '../constants/index';

export default {
  getUsers: () => axios.get(`${API_URL}/users`)
}